// imports
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { useNavigate, Navigate, Link } from 'react-router-dom';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';

export default function Orders() {

	const { user } = useContext(UserContext);

    const [orders, setOrders] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {

        (user.id) ?
            (user.isAdmin === false) ?
                fetch(`${process.env.REACT_APP_API_URL}/orders/done`, {
                    headers : {
                        Authorization : `Bearer ${ localStorage.getItem('token') }`
                    }   
                })
                    .then(res => res.json())
                    .then(data => {

                        setOrders(data.map(order => {
                            return (
                                <OrderCard key={ order._id } order={ order } />
                            )
                        }))
                    })
            :
                fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
                    headers : {
                        Authorization : `Bearer ${ localStorage.getItem('token') }`
                    }   
                })
                .then(res => res.json())
                .then(data => {

                    setOrders(data.map(order => {
                        return (
                            <OrderCard key={ order._id } order={ order } />
                        )
                    }))
                })
        :
            navigate("/login")
    }, [])
    
    return (
    	
        (user.id) ?
        	<>
        		<Container className="mt-2">
        		    <Link to="/products">&lt; Back to Products</Link>
        		    {
    		    		<>
                            { orders }
                        </>
        		    }
        		</Container>
        	</>
        :
        	<>
        		<Navigate to="/login" />
        	</>
    )
}