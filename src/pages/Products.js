import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {

    const { user } = useContext(UserContext);

    const [products, setProducts] = useState([]);

    useEffect(() => {

        (user.isAdmin === false || user.id === null) ?
            fetch(`${process.env.REACT_APP_API_URL}/products/active`)
                .then(res => res.json())
                .then(data => {

                    setProducts(data.map(product => {
                        return (
                            <ProductCard key={ product._id } product={ product } />
                        )
                    }))
                })
        :
            fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
                headers : {
                    Authorization : `Bearer ${ localStorage.getItem('token') }`
                }   
            })
            .then(res => res.json())
            .then(data => {

                setProducts(data.map(product => {
                    return (
                        <ProductCard key={ product._id } product={ product } />
                    )
                }))
            })
    }, [])

    return (
        <>
            <Container className="mt-3">
            {
                (user.id === null) ?
                    <>
                        { products }
                    </>
                :
                    (user.isAdmin===false) ?
                        <>
                            <Link className="btn btn-primary my-3 mx-1" to="/orders">Orders</Link>
                            <Link className="btn btn-info my-3 mx-1 disabled" to="/cart">Cart</Link>
                            <Link className="btn btn-secondary my-3 mx-1" to="/profile">Profile</Link>
                            { products }
                        </>
                    :
                        <>
                            <Link className="btn btn-primary my-3 mx-1" to="/product/create">Add Product</Link>
                            <Link className="btn btn-info my-3 mx-1" to="/orders">Transactions</Link>
                            <Link className="btn btn-secondary my-3 mx-1" to="/users">User Database</Link>
                            { products }
                        </>
            }
            </Container>
        </>
    )
}