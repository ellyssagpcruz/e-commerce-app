// imports
import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [email, setEmail] = useState('');
    const [completeAddress, setCompleteAddress] = useState('');

    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [isActive, setIsActive] = useState(false);

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        setPasswordShown(!passwordShown);
    };

    function registerUser(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                firstName : firstName,
                lastName : lastName,
                mobileNumber : mobileNumber,
                completeAddress : completeAddress,
                email : email,
                password : password1
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {

                Swal.fire({
                  icon: 'success',
                  title: 'Registration successful!',
                  text: 'Welcome to Cybershop!'
                })

                navigate("/login");

                setFirstName('');
                setLastName('');
                setMobileNumber('');
                setEmail('');
                setCompleteAddress('');
                setPassword1('');
                setPassword2('');
            } else {

              Swal.fire({
                icon: 'error',
                title: 'Duplicate email found.',
                text: 'Please provide a different email.'
              })
            }
        })
    }

    useEffect(() => {
        if((firstName !== "" && lastName !== "" && mobileNumber !== "" && completeAddress !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNumber, completeAddress, email, password1, password2]);

    return (
        <>
            <Container className="mt-3">
            {
                (user.id) ?
                    <Navigate to="/products" />
                :
                    <Form onSubmit={(e) => registerUser(e)}>
                        {/*NAMES*/}
                        <Form.Group controlId="userFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your first name"
                                value={ firstName }
                                onChange={e => setFirstName(e.target.value)}
                                required
                             />
                        </Form.Group>
                        <Form.Group controlId="userLastName">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your last name"
                                value={ lastName }
                                onChange={e => setLastName(e.target.value)}
                                required
                             />
                        </Form.Group>

                        {/*MOBILE NUMBER*/}
                        <Form.Group controlId="userMobileNumber">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter a mobile number"
                                value={ mobileNumber }
                                onChange={e => setMobileNumber(e.target.value)}
                                maxLength="11"
                                required
                             />
                        </Form.Group>

                        {/*COMPLETE ADDRESS*/}
                        <Form.Group controlId="userCompleteAddress">
                            <Form.Label>Complete Address</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your complete address"
                                value={ completeAddress }
                                onChange={e => setCompleteAddress(e.target.value)}
                                required
                             />
                        </Form.Group>

                        {/*EMAIL*/}
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter an email"
                                value={ email }
                                onChange={e => setEmail(e.target.value)}
                                required
                             />
                             <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
                        </Form.Group>

                        {/*PASSWORDS*/}
                        <Form.Group controlId="password1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type={passwordShown ? "text" : "password"}
                                placeholder="Enter a password"
                                value={ password1 }
                                onChange={e => setPassword1(e.target.value)}
                                required
                             />
                        </Form.Group>
                        <Form.Group controlId="password2" className="mb-3">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type={passwordShown ? "text" : "password"}
                                placeholder="Confirm your password"
                                value={ password2 }
                                onChange={e => setPassword2(e.target.value)}
                                required
                             />
                             <Form.Text className="text-muted">Make sure your password are the same in order to proceed.</Form.Text>
                            <Form.Check
                               type="switch"
                               label="Reveal and Compare Passwords"
                               onChange={togglePassword}
                               className="mt-1"
                            />
                        </Form.Group>
                        
                        {
                            isActive ?
                                <Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
                            :
                                <Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
                        }
                    </Form>
            }
            </Container>
        </>
    )
}