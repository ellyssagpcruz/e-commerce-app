// imports
import React from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Featured from '../components/Featured';

export default function Home() {

    const data = {
        title: "Welcome to Cybershop",
        content: "Where all tech junkies meet.",
        destination: "/products",
        label: "View Catalogue"
    }


    return (
        <>
            <Container className="mt-3">
                <Banner data={data}/>
                <Featured />
            </Container>
        </>
    )
}