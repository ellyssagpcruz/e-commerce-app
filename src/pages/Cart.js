// imports
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Cart() {

	const { user } = useContext(UserContext);

    const navigate = useNavigate();
    
    return (

        (user.id) ?
        	<>
        		<Container className="mt-2">
        		    <Link to="/products">&lt; Back to Products</Link>
        		    {
        		    	(user.isAdmin===false) ?
        		    		<>
        		    		</>
        		    	:
        		    		<>
        		    		</>
        		    }
        		</Container>
        	</>
        :
        	<>
        		<Navigate to="/products" />
        	</>
    )
}