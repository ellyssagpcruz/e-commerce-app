// imports
import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
    
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        setPasswordShown(!passwordShown);
    };

    function loginUser(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email,
                password : password
            })
        })
        .then(res => res.json())
        .then(data => {

            if (typeof data.access !== "undefined") {

                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login successful!',
                  text: 'Welcome to Cybershop!'
                })
            } else {

          Swal.fire({
            icon: 'error',
            title: 'Authentication failed',
            text: 'Please check your login credentials and try again.'
          })
        }
        })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers : {
                Authorization : `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setUser({
                id : data._id,
                email : data.email,
                isAdmin : data.isAdmin
            })
        })
    }

    useEffect(() => {

        if(email !== "" && password !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]);

    return (
        <>
            <Container className="mt-3">
            {
                (user.id) ?
                     <Navigate to="/products" />
                :
                    <Form onSubmit={(e) => loginUser(e)}>
                     <Form.Group controlId="userEmail">
                         <Form.Label>Email Address</Form.Label>
                         <Form.Control
                             type="email"
                             placeholder="Enter an email"
                             value={ email }
                             onChange={e => setEmail(e.target.value)}
                             required
                          />
                     </Form.Group>

                     <Form.Group controlId="password" className="mb-3">
                         <Form.Label>Password</Form.Label>
                         <Form.Control
                             type={passwordShown ? "text" : "password"}
                             placeholder="Enter a password"
                             value={ password }
                             onChange={e => setPassword(e.target.value)}
                             required
                         />
                         <Form.Check
                            type="switch"
                            label="Show Password"
                            onChange={togglePassword}
                            className="mt-1"
                         />
                     </Form.Group>

                     {
                         isActive ?
                             <Button variant="success" type="submit" id="submitBtn" className="mt-2">Login</Button>
                             :
                             <Button variant="success" type="submit" id="submitBtn" className="mt-2" disabled>Login</Button>
                     }
                    </Form> 
            }
            </Container>
        </>
    )
}