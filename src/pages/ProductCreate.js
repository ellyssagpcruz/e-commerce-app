import React from 'react';
import { useState, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductCreate() {

    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [brand, setBrand] = useState("");
    const [sku, setSKU] = useState("");
    const [stocks, setStocks] = useState(1);

    function createProduct(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                name : name,
                description : description,
                brand : brand,
                sku : sku,
                stocks : stocks,
                price : price
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {

                Swal.fire({
                  icon: 'success',
                  title: 'Successfully added product',
                  text: 'You have successfully added this product.'
                })

                navigate("/products")
            } else {

              Swal.fire({
                icon: 'error',
                title: 'Something went wrong.',
                text: 'Please try again.'
              })
            }
        })
    }

    return (
        (user.id) ?
            (user.isAdmin === false) ?
                navigate("/products")
            :
                <Container className="mt-2">
                    <Link to="/products">&lt; Back to Products</Link>
                    <Row>
                        <Col lg={{ span: 6, offset: 3 }}>
                            <Card className="mt-3">
                                <Card.Body className="text-center">
                                    <Form onSubmit={(e) => createProduct(e)}>
                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productName">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ name }
                                            onChange={e => setName(e.target.value)}
                                            required
                                         />
                                    </Form.Group>

                                    <Form.Group className="mb-2 w-50 m-auto" controlId="productDescription">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ description }
                                            onChange={e => setDescription(e.target.value)}
                                            required
                                         />
                                    </Form.Group>

                                    <Form.Group className="mb-2 w-50 m-auto" controlId="productBrand">
                                        <Form.Label>Brand</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ brand }
                                            onChange={e => setBrand(e.target.value)}
                                            required
                                         />
                                    </Form.Group>

                                    <Form.Group className="mb-2 w-50 m-auto" controlId="productSKU">
                                        <Form.Label>SKU</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ sku }
                                            onChange={e => setSKU(e.target.value)}
                                            required
                                         />
                                    </Form.Group>

                                    <Form.Group className="mb-2 w-50 m-auto" controlId="productStocks">
                                        <Form.Label>Stocks</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ stocks }
                                            onChange={e => setStocks(e.target.value)}
                                            required
                                         />
                                    </Form.Group>

                                    <Form.Group className="mb-3 w-50 m-auto" controlId="productPrice">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control
                                            type="text"
                                            value={ price }
                                            onChange={e => setPrice(e.target.value)}
                                            maxLength="6"
                                            required
                                         />
                                    </Form.Group>

                                        <Button variant="primary" type="submit" className="mx-1 mt-2">Create</Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>                
        :
            <Navigate to="/products" />
    )
}