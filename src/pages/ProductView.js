import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';

export default function ProductView() {

    const { productId } = useParams();

    const { user } = useContext(UserContext);

    const [orders, setOrders] = useState([]);

    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [brand, setBrand] = useState("");
    const [sku, setSKU] = useState("");
    const [stocks, setStocks] = useState(1);
    const [quantity, setQuantity] = useState(1);
    const [isActive, setIsActive] = useState(true);

    const [isSales, setIsSales] = useState(false);

    const [status, setStatus] = useState("");

    const order = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                productId : productId,
                quantity : quantity
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {
                Swal.fire({
                    icon : 'success',
                    title : 'Successfully checked out',
                    text : 'You have successfully checked out this product.'
                })

                navigate("/products")
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'Something went wrong',
                    text : 'Please try again.'
                })
            }
        })
    };

    const cart = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                quantity : quantity
            })
        })
        .then(res => res.json())
        .then(data => {

        })
    };

    const archive = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                isActive : false
            })
        })
        .then(res => res.json())
        .then(data => {

            setIsActive(false)

            if (data === true) {
                Swal.fire({
                    icon : 'success',
                    title : 'Successfully archived product',
                    text : 'You have successfully archived this product.'
                })

                navigate("/products");
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'Something went wrong',
                    text : 'Please try again.'
                })
            }
        })
    };

    const activate = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                isActive : true
            })
        })
        .then(res => res.json())
        .then(data => {

            setIsActive(true)

            if (data === true) {
                Swal.fire({
                    icon : 'success',
                    title : 'Successfully activated product',
                    text : 'You have successfully activated this product.'
                })

                navigate("/products");
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'Something went wrong',
                    text : 'Please try again.'
                })
            }
        })
    };

    const sales = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}/sales`, {
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }   
        })
        .then(res => res.json())
        .then(data => {

            if (data.length > 0) {

                setOrders(data.map(order => {
                    return (
                        <OrderCard key={ order._id } order={ order } />
                    )
                }));

                setIsSales(true);
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'No sales for this product',
                    text : 'Come again some other time.'
                })
            }
        })
    };

    function updateProduct(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`, {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                name : name,
                description : description,
                brand : brand,
                sku : sku,
                stocks : stocks,
                price : price
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {

                Swal.fire({
                  icon: 'success',
                  title: 'Successfully updated product',
                  text: 'You have successfully updated this product.'
                })
            } else {

              Swal.fire({
                icon: 'error',
                title: 'Something went wrong.',
                text: 'Please try again.'
              })
            }
        })
    }

    useEffect(() => {

        (user.isAdmin === false) ?
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {

                setName(data.name)
                setDescription(data.description)
                setBrand(data.brand)
                setPrice(data.price)
            })
        :
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
            .then(res => res.json())
            .then(data => {

                setName(data.name);
                setDescription(data.description);
                setBrand(data.brand);
                setSKU(data.sku);
                setStocks(data.stocks);
                setIsActive(data.isActive);
                setPrice(data.price);

                (data.isActive === true) ?
                    setStatus("Activated")
                :
                    setStatus("Archived")
            })
    }, [productId]);

    return (

        <>
            <Container className="mt-2">
                <Link to="/products">&lt; Back to Products</Link>
                <Row>
                    <Col lg={{ span: 6, offset: 3 }}>
                        <Card className="my-3">
                        <h2 className="text-center mt-3">Product Details</h2>
                            <Card.Body className="text-center">
                            {
                                (user.isAdmin===true) ?
                                    <Form onSubmit={(e) => updateProduct(e)}>
                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productName">
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ name }
                                                onChange={e => setName(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productDescription">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ description }
                                                onChange={e => setDescription(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productBrand">
                                            <Form.Label>Brand</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ brand }
                                                onChange={e => setBrand(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productSKU">
                                            <Form.Label>SKU</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ sku }
                                                onChange={e => setSKU(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-2 w-50 m-auto" controlId="productStocks">
                                            <Form.Label>Stocks</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ stocks }
                                                onChange={e => setStocks(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-3 w-50 m-auto" controlId="productPrice">
                                            <Form.Label>Price</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ price }
                                                onChange={e => setPrice(e.target.value)}
                                                maxLength="6"
                                                required
                                             />
                                        </Form.Group>

                                        <Form.Group className="mb-3 w-50 m-auto" controlId="productStatus">
                                            <Form.Label>Status: { status }</Form.Label>
                                        </Form.Group>

                                        <Button variant="primary" type="submit" className="mx-1 mt-2">Update</Button>
                                        {
                                            (status==="Activated") ?
                                                <Button variant="warning" className="mx-1 mt-2" onClick={() => archive(productId)}>Archive</Button>
                                            :    
                                                <Button variant="success" className="mx-1 mt-2" onClick={() => activate(productId)}>Activate</Button>
                                        }
                                        <Button variant="info" className="mx-1 mt-2" onClick={() => sales(productId)}>Show Sales</Button>
                                    </Form>
                                :
                                    <>
                                        <Card.Title className="mb-3">{ name }</Card.Title>
                                        <Card.Subtitle>Description:</Card.Subtitle>
                                        <Card.Text>{ description }</Card.Text>
                                        <Card.Subtitle>Brand:</Card.Subtitle>
                                        <Card.Text>{ brand }</Card.Text>
                                        <Card.Subtitle>Price:</Card.Subtitle>
                                        <Card.Text>Php { price }</Card.Text>
                                        {
                                            (user.id) ?
                                                <>
                                                    <Form.Group className="mb-3">
                                                        <Card.Subtitle className="mb-2">Quantity:</Card.Subtitle>
                                                        <Form.Control
                                                            type="text"
                                                            value={ quantity }
                                                            onChange={e => setQuantity(e.target.value)}
                                                            className="w-25 m-auto text-center"
                                                         />
                                                    </Form.Group>

                                                    <Button variant="primary" className="mx-1 mt-2" onClick={() => order(productId)}>Buy Now</Button>
                                                    <Button variant="info" className="mx-1 mt-2" onClick={() => cart(productId)} disabled>Add to Cart</Button>
                                                </>
                                            :
                                                <Link className="btn btn-danger btn-block" to="/login">Log in to order</Link>
                                        }
                                    </>
                            }
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                {
                    (isSales === true) ?
                        <Row>
                            <Col>
                                <h2 className="text-center mt-3">Product Sales</h2>
                                { orders }
                            </Col>
                        </Row>
                    :
                        <>
                        </>
                } 
            </Container>
        </>
    )
}