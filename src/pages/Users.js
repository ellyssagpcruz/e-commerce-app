// imports
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserCard from '../components/UserCard';
import UserContext from '../UserContext';

export default function Users() {

	const { user } = useContext(UserContext);

    const [accounts, setAccounts] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }   
        })
        .then(res => res.json())
        .then(data => {

            setAccounts(data.map(account => {
                return (
                    <UserCard key={ account._id } account={ account } />
                )
            }))
        })
    }, [])
    
    return (

    	(user.id && user.isAdmin === true) ?
    		<>
    			<Container className="mt-2 mb-5">
    			    <Link to="/products">&lt; Back to Products</Link>
    			    {
                        <>
                            { accounts }
                        </>
    			    }
    			</Container>
    		</>
    	:
    		<>
    			<Navigate to="/products" />
    		</>
    )
}