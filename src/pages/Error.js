// imports
import React from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <>
            <Container className="mt-3">
                <Banner data={data}/>
            </Container>
        </>
    )
}
