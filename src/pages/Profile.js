// imports
import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Profile() {

	const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [email, setEmail] = useState('');
    const [completeAddress, setCompleteAddress] = useState('');

    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePassword = () => {
        setPasswordShown(!passwordShown);
    };

    function updateUserDetails(e) {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                firstName : firstName,
                lastName : lastName,
                mobileNumber : mobileNumber,
                completeAddress : completeAddress,
                email : email
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {

                Swal.fire({
                  icon: 'success',
                  title: 'Successfully updated user details',
                  text: 'You have successfully your user details.'
                })
            } else {

              Swal.fire({
                icon: 'error',
                title: 'Something went wrong.',
                text: 'Please try again.'
              })
            }
        })
    }

    useEffect(() => {

        (user.id && user.isAdmin===false) ?
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers : {
                    Authorization : `Bearer ${ localStorage.getItem('token') }`
                }
            })
            .then(res => res.json())
            .then(data => {

                setFirstName(data.firstName);
                setLastName(data.lastName);
                setMobileNumber(data.mobileNumber);
                setEmail(data.email);
                setCompleteAddress(data.completeAddress);
            })
        :
            navigate("/products")
    }, [password1, password2]);
    
    return (

    	(user.isAdmin===false && user.id) ?
    		<>
    			<Container className="mt-2">
    			    <Link to="/products">&lt; Back to Products</Link>
                    <Row>
                        <Col lg={{ span: 6, offset: 3 }}>
                            <Card className="mt-3">
                                <Card.Body>
                                    <Form onSubmit={(e) => updateUserDetails(e)}>
                                        {/*NAMES*/}
                                        <Form.Group controlId="userFirstName">
                                            <Form.Label>First Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ firstName }
                                                onChange={e => setFirstName(e.target.value)}
                                                required
                                             />
                                        </Form.Group>
                                        <Form.Group controlId="userLastName">
                                            <Form.Label>Last Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ lastName }
                                                onChange={e => setLastName(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        {/*MOBILE NUMBER*/}
                                        <Form.Group controlId="userMobileNumber">
                                            <Form.Label>Mobile Number</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ mobileNumber }
                                                onChange={e => setMobileNumber(e.target.value)}
                                                maxLength="11"
                                                required
                                             />
                                        </Form.Group>

                                        {/*COMPLETE ADDRESS*/}
                                        <Form.Group controlId="userCompleteAddress">
                                            <Form.Label>Complete Address</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={ completeAddress }
                                                onChange={e => setCompleteAddress(e.target.value)}
                                                required
                                             />
                                        </Form.Group>

                                        {/*EMAIL*/}
                                        <Form.Group controlId="userEmail">
                                            <Form.Label>Email Address</Form.Label>
                                            <Form.Control
                                                type="email"
                                                value={ email }
                                                onChange={e => setEmail(e.target.value)}
                                                disabled
                                             />
                                        </Form.Group>

                                        {/*PASSWORDS*/}
                                        <Form.Group controlId="password1">
                                            <Form.Label>New Password</Form.Label>
                                            <Form.Control
                                                type={passwordShown ? "text" : "password"}
                                                placeholder="Enter new password"
                                                value={ password1 }
                                                onChange={e => setPassword1(e.target.value)}
                                             />
                                        </Form.Group>
                                        <Form.Group controlId="password2" className="mb-3">
                                            <Form.Label>Confirm New Password</Form.Label>
                                            <Form.Control
                                                type={passwordShown ? "text" : "password"}
                                                placeholder="Confirm new password"
                                                value={ password2 }
                                                onChange={e => setPassword2(e.target.value)}
                                             />
                                             <Form.Text className="text-muted">Make sure your password are the same in order to proceed.</Form.Text>
                                            <Form.Check
                                               type="switch"
                                               label="Reveal and Compare Passwords"
                                               onChange={togglePassword}
                                               className="mt-1"
                                            />
                                        </Form.Group>
                                        
                                        <Button variant="primary" type="submit" id="submitBtn" className="mt-2">Update</Button>
                                    </Form>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
    			</Container>
    		</>
    	:
    		<>
    			<Navigate to="/products" />
    		</>
    )
}