import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({ product }) {

    const { user } = useContext(UserContext);

	const { _id, name, description, price, isActive } = product;

	return(

        (user.isAdmin===false) ?
            <Card className="mb-3">
                <Card.Body>
                    <Card.Title>{ name }</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{ description }</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP { price }</Card.Text>
                    <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
                </Card.Body>
            </Card>
        :
            <>
                <Card className="mb-3">
                    <Card.Body>
                        <Card.Title>{ name }</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{ description }</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP { price }</Card.Text>
                        {
                            (isActive===true) ?
                                <Link className="btn btn-success" to={`/product/${_id}`}>Details</Link>
                            :
                                <Link className="btn btn-warning" to={`/product/${_id}`}>Details</Link>

                        }
                    </Card.Body>
                </Card>
            </>
	)
}