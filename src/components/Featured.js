import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Featured() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4} className="mb-1">
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Featured Product #1</h2>
						</Card.Title>
						<Card.Text>
							Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
						</Card.Text>
						<Card.Subtitle className="mt-3 mb-5">Price: 0.00</Card.Subtitle>
						<Link className="btn btn-primary disabled" to="">Details</Link>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Featured Product #2</h2>
						</Card.Title>
						<Card.Text>
							Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
						</Card.Text>
						<Card.Subtitle className="mt-3 mb-5">Price: 0.00</Card.Subtitle>
						<Link className="btn btn-primary disabled" to="">Details</Link>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Featured Product #3</h2>
						</Card.Title>
						<Card.Text>
							Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
						</Card.Text>
						<Card.Subtitle className="mt-3 mb-5">Price: 0.00</Card.Subtitle>
						<Link className="btn btn-primary disabled" to="">Details</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}