import React from 'react';
import { useState, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function UserCard({ account }) {

	const { user } = useContext(UserContext);

	const { _id, firstName, lastName, mobileNumber, completeAddress, email, isAdmin } = account;

	const navigate = useNavigate();

	const toAdmin = (userEmail) => {

	    fetch(`${process.env.REACT_APP_API_URL}/users/admin`, {
	        method : "PATCH",
	        headers : {
	            "Content-Type" : "application/json",
	            Authorization : `Bearer ${ localStorage.getItem('token') }`
	        },
	        body : JSON.stringify({
	        	email : userEmail,
	            isAdmin : true
	        })
	    })
	    .then(res => res.json())
	    .then(data => {

	        if (data === true) {
	            Swal.fire({
	                icon : 'success',
	                title : 'Successfully added admin access',
	                text : `You have successfully added admin access for ${userEmail}.`
	            })

	            navigate("/products");
	        } else {
	            Swal.fire({
	                icon : 'error',
	                title : 'Something went wrong',
	                text : 'Please try again.'
	            })
	        }
	    })
	};

	const fromAdmin = (userEmail) => {
	    fetch(`${process.env.REACT_APP_API_URL}/users`, {
	        method : "PATCH",
	        headers : {
	            "Content-Type" : "application/json",
	            Authorization : `Bearer ${ localStorage.getItem('token') }`
	        },
	        body : JSON.stringify({
	            email : userEmail,
	            isAdmin : false
	        })
	    })
	    .then(res => res.json())
	    .then(data => {

	        if (data === true) {
	            Swal.fire({
	                icon : 'success',
	                title : 'Successfully removed admin access',
	                text : `You have successfully removed admin access for ${userEmail}.`
	            })

	            navigate("/products");
	        } else {
	            Swal.fire({
	                icon : 'error',
	                title : 'Something went wrong',
	                text : 'Please try again.'
	            })
	        }
	    })
	};

	return(
		<>
			<Container className="mt-2">
			    <Row>
			        <Col lg={{ span: 6, offset: 3 }}>
			            <Card className="mt-3">
			                <Card.Body className="text-center">
	                                <Card.Title className="mb-3">{ firstName } { lastName }</Card.Title>
	                                <Card.Subtitle>Mobile Number:</Card.Subtitle>
	                                <Card.Text>{ mobileNumber }</Card.Text>
	                                <Card.Subtitle>Complete Address:</Card.Subtitle>
	                                <Card.Text>{ completeAddress }</Card.Text>
	                                <Card.Subtitle>Email:</Card.Subtitle>
	                                <Card.Text>{ email }</Card.Text>
	                                {
                                        (isAdmin===false) ?
                                            <Button variant="warning" className="mx-1 mt-2" onClick={() => toAdmin(email)}>Give Admin Access</Button>
                                        :    
                                        	(user.email === email) ?
                                        		<Button variant="success" className="mx-1 mt-2" onClick={() => fromAdmin(email)} disabled>Remove Admin Access</Button>
                                        	:
                                        		<Button variant="success" className="mx-1 mt-2" onClick={() => fromAdmin(email)}>Remove Admin Access</Button>
                                    }  
	                    	</Card.Body>
	                	</Card>
	            	</Col>
	        	</Row>
	    	</Container>

		</>
	)
}