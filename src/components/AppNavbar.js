import React from 'react';
import { useContext } from 'react';

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	const goToTop = () =>{
	    window.scrollTo({
	      top: 0, 
	      behavior: 'smooth'
	    });
	  };

	return(

		<Navbar id="top" bg="light" expand="lg" className="sticky-top navbar-dark bg-dark">
			<Container fluid>
				<Navbar.Brand onClick={() => goToTop()}>Cybershop</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						{
							(user.id) ?
								(user.isAdmin === true) ?
									<Navbar.Text className="me-5 fw-bold">Hello Admin!</Navbar.Text>
								:
									<Navbar.Text className="me-5 fw-bold">Hello Customer!</Navbar.Text>
							:
								<>
								</>
						}
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
						{
							(user.id) ?
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}
