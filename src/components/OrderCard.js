import React from 'react';
import { useEffect, useState, useContext } from 'react';
import { Card } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function OrderCard({ order }) {

	const { user } = useContext(UserContext);

	const { _id, userId, products, totalAmount, purchasedOn, status } = order;

	const { productId, quantity, subTotal } = products;

    const navigate = useNavigate();

	const [userEmail, setUserEmail] = useState('');
    const [productName, setProductName] = useState('');

    function getUserEmail (userId) {
    	fetch(`${process.env.REACT_APP_API_URL}/users/user`, {
    		method : "POST",
    	    headers : {
    	        "Content-Type" : "application/json",
    	        Authorization : `Bearer ${ localStorage.getItem('token') }`
    	    },
    	    body : JSON.stringify({
    	        id : userId.userId
    	    })
    	})
    	.then(res => res.json())
    	.then(data => {

    		setUserEmail(data.email)
    	})

    	return userEmail
    };

    function getProductName (prodId) {
    	fetch(`${process.env.REACT_APP_API_URL}/products/${prodId}`)
    	.then(res => res.json())
    	.then(data => {
    		setProductName(data.name)
    	})

    	return productName
    };

    const cancel = (orderId) => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/cancel`, {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                status : "Cancelled"
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {
                Swal.fire({
                    icon : 'success',
                    title : 'Successfully cancelled order',
                    text : `You have successfully cancelled this order.`
                })

                navigate("/products");
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'Something went wrong',
                    text : 'Please try again.'
                })
            }
        })
    };

    const complete = (orderId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/done`, {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            },
            body : JSON.stringify({
                status : "Completed"
            })
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {
                Swal.fire({
                    icon : 'success',
                    title : 'Successfully completed order',
                    text : `You have successfully completed this order.`
                })

                navigate("/products");
            } else {
                Swal.fire({
                    icon : 'error',
                    title : 'Something went wrong',
                    text : 'Please try again.'
                })
            }
        })
    };

	return(
		(user.isAdmin===false) ?
            <Card className="my-3">
            	<Card.Header>Order Purchased On: { purchasedOn }</Card.Header>
                <Card.Body>
                	<Card.Subtitle>Items:</Card.Subtitle>
                	<ul>
                		{
                			products.map(product=> (
                		        <li key={ product }>{ getProductName(product.productId) } | Quantity: { product.quantity } | Subtotal: { product.subTotal }</li>
                		    ))
                		}
                	</ul>                
                    <Card.Subtitle>Total:</Card.Subtitle>
                    <Card.Text>PhP { totalAmount }</Card.Text>                    
                </Card.Body>
                <Card.Footer className="d-flex" style={{height : "40.8px"}}>
                    <Card.Text className="me-auto text-muted">Status: { status }</Card.Text>
                    {
                        (status !== "Cancelled" && status !== "Completed") ?
                            <>
                                <Link className="ms-3" onClick={() => cancel(_id)}>Cancel Order</Link>
                                <Link className="ms-3" onClick={() => complete(_id)}>Order Received</Link>
                            </>
                        :
                            <>
                            </>
                    } 
                </Card.Footer>
            </Card>
        :
            <>
                <Card className="my-3">
                	<Card.Header>Order Purchased On: { purchasedOn }</Card.Header>
                    <Card.Body>
                    	<Card.Subtitle>Purchased By:</Card.Subtitle>
                    	<Card.Text>{ getUserEmail({userId}) }</Card.Text>
                    	<Card.Subtitle>Items:</Card.Subtitle>
                    	<ul>
                    		{
                    			products.map(product=> (
                    		        <li key={ product }>{ getProductName(product.productId) } | Quantity: { product.quantity } | Subtotal: { product.subTotal }</li>
                    		    ))
                    		}
                    	</ul>                    	
                        <Card.Subtitle>Total:</Card.Subtitle>
                        <Card.Text>PhP { totalAmount }</Card.Text>
                    </Card.Body>
                    <Card.Footer>Status: { status }</Card.Footer>
                </Card>
            </>
	)
}