// imports from packages
import React from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

// import navbar
import AppNavbar from './components/AppNavbar';

// imports from routes
import Cart from './pages/Cart';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Orders from './pages/Orders';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import ProductCreate from './pages/ProductCreate';
import Profile from './pages/Profile';
import Register from './pages/Register';
import Users from './pages/Users';

// import user content
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({ id : null, email : null, isAdmin : null});

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {

    if (localStorage.getItem('token')) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers : {
              Authorization : `Bearer ${ localStorage.getItem('token') }`
          }
      })
      .then(res => res.json())
      .then(data => {

          if(data._id) {
            setUser({
                id: data._id,
                email : data.email,
                isAdmin : data.isAdmin
            })
          }
      })
    };

  }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="/" element={ <Home /> } />
            <Route path="/products" element={ <Products /> } />
            <Route path="/product/:productId" element={<ProductView />} />  
            <Route path="/product/create" element={<ProductCreate />} />  
            <Route path="/orders" element={<Orders />} />  
            <Route path="/cart" element={<Cart />} />  
            <Route path="/profile" element={<Profile />} />  
            <Route path="/users" element={<Users />} />  
            <Route path="/register" element={ <Register /> } />
            <Route path="/login" element={ <Login /> } />
            <Route path="/logout" element={ <Logout /> } />
            <Route path="/*" element={ <Error /> } />
          </Routes>
        </Container>    
      </Router>
    </UserProvider>
  )
}

export default App;
